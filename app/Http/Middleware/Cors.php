<?php

namespace App\Http\Middleware;

use Closure;

class Cors
{
    public function handle($request, Closure $next)
    {

//        if ($request->isMethod('OPTIONS')) {
//
//            return $next($request)->header('Access-Control-Allow-Origin', '*')
//                ->header('Access-Control-Allow-Methods', 'POST, GET');
//        }
//
//            return $next($request)
//            ->header('Access-Control-Allow-Headers', 'accept, content-type, Origin, Content-Type, Autorization')
//            ->header('Access-Control-Allow-Origin', '*')
//            ->header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, DELETE, PUT, PATCH');





        return $next($request)
//            ->header('Access-Control-Allow-Origin', 'http://frontsite')
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With')
            ->header('Access-Control-Expose-Headers', 'Authorization')
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');

    }
}
