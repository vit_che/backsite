<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Article;

class ArticleController extends Controller
{
    public function getArticles(){

        // setcookie('key', "test_KEY");
        // setcookie('nota', "nota_KEY");

        $articles = Article::all();
        // var_dump($articles);

        return $articles;
    }

    public function addArticle(Request $request){

//        if ($request->isMethod('post')) {

            $input = $request->except('_token');
            
            $article = new Article();

            $article->fill($input);

            if ($article->save()) {

                $message = "Событие Сохранено!";

                return $message;
                // return $input;
            }
//        }
    }

    public function delArticle(Request $request){

        $id = $request->id;

        $article_del = Article::find($id);

        $article_del->delete();

        $message = "Событие!".$id;

        return $message;


    }
}
