<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/vadim', function () {

    return url('http://backsite/vadim/index.html');
})->name('vadim');
// Route::get('/vadim/main.js', function () {

//     return url('http://backsite/vadim/main.js');
// })->name('main');

Route::get('/getArticles', ['uses' => 'ArticleController@getArticles', 'as' => 'getArticles']);


Route::group(['middleware'=>['web']], function (){

    Route::get('/getArticles', ['uses' => 'ArticleController@getArticles', 'as' => 'getArticles']);

    // Route::match(['get', 'post'], '/addArticle', ['uses' => 'ArticleController@addArticle', 'as' => 'addArticle']);
});

Route::post('/addArticle', ['uses' => 'ArticleController@addArticle', 'as' => 'addArticle']);

Route::post('/delArticle', ['uses' => 'ArticleController@delArticle', 'as' => 'delArticle']);



Route::options('{any}', ['middleware' => ['cors'], function () {

    return response(['status' => 'success']);

}])->where('any', '.*');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
